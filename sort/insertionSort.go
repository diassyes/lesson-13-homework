package sort

type InsertionSort struct {
}

func (s *InsertionSort) Sort(arr []int) {
	size := len(arr)
	if size < 2 {
		return
	}
	for i := 1; i < size; i++ {
		var j int
		var buff = arr[i]
		for j = i - 1; j >= 0; j-- {
			if arr[j] < buff {
				break
			}
			arr[j+1] = arr[j]
		}
		arr[j+1] = buff
	}
}
