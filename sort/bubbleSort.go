package sort

type BubbleSort struct {
}

func (s *BubbleSort) Sort(arr []int) {
	size := len(arr)
	if size < 2 {
		return
	}
	for i := 0; i < size; i++ {
		for j := size - 1; j >= i+1; j-- {
			if arr[j] < arr[j-1] {
				arr[j], arr[j-1] = arr[j-1], arr[j]
			}
		}
	}
}
