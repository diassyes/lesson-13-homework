// Implementing Strategy Pattern
package main

import (
	"fmt"
	"lesson-13-homework/sort"
	"strconv"
)

// Provides an interface for sort algorithms
type SortStrategy interface {
	Sort([]int)
}

// Provides a context for execution of a strategy
type Context struct {
	strategy SortStrategy
}

// Replaces strategies
func (c *Context) Algorithm(sortStrategy SortStrategy) {
	c.strategy = sortStrategy
}

// Sorts data according to the chosen strategy
func (c *Context) Sort(arr []int) {
	c.strategy.Sort(arr)
}


func main() {

	ctx := Context{}

	arr1 := []int{8, 2, 6, 7, 1, 3, 9, 5, 4}
	arr2 := []int{8, 2, 6, 7, 1, 3, 9, 5, 4}
	arr3 := []int{8, 2, 6, 7, 1, 3, 9, 5, 4}

	ctx.Algorithm(&sort.BubbleSort{})
	ctx.Sort(arr1)
	PrintString(arr1)

	ctx.Algorithm(&sort.QuickSort{})
	ctx.Sort(arr2)
	PrintString(arr2)

	ctx.Algorithm(&sort.InsertionSort{})
	ctx.Sort(arr3)
	PrintString(arr3)

}

func PrintString(arr []int) {
	var result string
	for _, val := range arr {
		result += strconv.Itoa(val) + ","
	}
	fmt.Println(result)
}
